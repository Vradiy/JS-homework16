let buttonColor = document.getElementById('changeColor');

const ready = function() {
    if(localStorage.getItem('bcgcolor') !== null) {
        let color = localStorage.getItem('bcgcolor');
        document.body.style.background = color;
    }
    
    buttonColor.onclick = function() {
        let bcgclr = document.body.style.background;
    if(bcgclr === 'pink') {
        document.body.style.background = 'white';
        localStorage.setItem('bcgcolor', 'white');
    } else {
        document.body.style.background = 'pink';
        localStorage.setItem('bcgcolor', 'pink')
    }

} 
}
document.addEventListener('DOMContentLoaded', ready);
